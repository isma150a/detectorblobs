function result = detectBlobFeatures(prev_img, img, level_threshold, K, angle, len)


if (nargin<1), prev_img = imread('cameraman.tif'); end
if (nargin<2), img = []; end
if (nargin<3), level_threshold=-1; end
if (nargin<4), K=4; end

if (nargin<5), angle=0; end
if (nargin<6), len=10; end

% I = imread('kids.tif'); 
% I = imread('moon.tif');
% I = imread('board.tif'); 
% I = imread('spine.tif');
% I = imread('trees.tif');

% Simulated movement
if (size(img,1)==0)
    H = fspecial('motion',len,angle);
    img = imfilter(prev_img,H,'conv','circular');

%     % Rectangle to simulated local movement
%     x=70;
%     y=15;
%     width=100;
%     height=80;
%     img=prev_img;
%     img(y:y+height,x:x+width) = imfilter(prev_img(y:y+height,x:x+width),H,'conv','circular');
end

if (ndims(prev_img)>2)
    prev_img=rgb2gray(prev_img);
end

if (ndims(img)>2)
    img=rgb2gray(img);
end


%% 
diff = abs(prev_img - img);
if (level_threshold == -1)
    level = graythresh(diff); %Otsu's method
else
    level = level_threshold;
end

% Apply a threshold
diff_bw = im2bw(diff, level);

% Obtain the regions
info = regionprops(diff_bw,'Area','Centroid');

% Find positions of maximun areas
positions = findKLargestAreaRegionprops(info, K);

%% K maximun areas of blobs
k_areas = zeros (1, K);
for i=1:K
    if (positions(i) ~= -1)
        k_areas(i) = info(positions(i)).Area;
    end
end

% Normalize these areas
sum_k_areas = sum(k_areas);
k_areas = k_areas ./ sum_k_areas;

%% (K-1)! distances between centroids of blobs
dist_centroid = zeros(1, summation(K-1));
count = 1;
for i=1:K
    if (positions(i) ~= -1)
        % Point A
        x1 = info(positions(i)).Centroid(1);
        y1 = info(positions(i)).Centroid(2);
        for j=i+1:K
            if (positions(j) ~= -1)
                % Point B
                x2 = info(positions(j)).Centroid(1);
                y2 = info(positions(j)).Centroid(2);
                % Distance between the two points
                dist_centroid(count) = sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );
                count = count + 1;
            end
        end
    end
end

% Normalize these distances between centroids
sum_dist_centroid = sum(dist_centroid);
dist_centroid = dist_centroid ./ sum_dist_centroid;

%%



result = [k_areas dist_centroid];


end