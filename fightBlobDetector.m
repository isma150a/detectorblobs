function fightBlobDetector(path_videos_fight, path_videos_nofight, reSize, parallelize,  level_threshold, K, M, maxFiles)
fprintf('\n fightBlobDetector-v1 \n');

warning off;
warning off;

% addpath('../Proyecto/Otros/Random Forest Mila-Gloria/');  % RandomForest classifier, Mila+Gloria
% addpath('../Proyecto/Otros/');
% addpath('../Proyecto/Otros/toolbox/mmread/');
% addpath('../Proyecto/Otros/toolbox/prtools/');
% addpath('../Proyecto/Otros/toolbox/prtools5/');

if (nargin<1), path_videos_fight='D:\Peliculas\fights\*.avi'; end
% if (nargin<1), path_videos_fight='C:\Peliculas\fights\*.avi'; end
% if (nargin<1), path_videos_fight='E:\Hockey\fights\*.avi'; end
% if (nargin<1), path_videos_fight='C:\Hockey\fights\*.avi'; end
% if (nargin<1), path_videos_fight='C:\UCF-101\fights\*.avi'; end
% if (nargin<1), path_videos_fight='E:\UCF-101\figths\*.avi'; end
% if (nargin<1), path_videos_fight='E:\UCF-101-pru500\fi*.avi'; end
% if (nargin<1), path_videos_fight='C:\UCF-101-pru500\fi*.avi'; end
% if (nargin<1), path_videos_fight='D:\HockeyPru\fights\*.avi'; end

if (nargin<2), path_videos_nofight='D:\Peliculas\noFights\*.mpg'; end
% if (nargin<2), path_videos_nofight='C:\Peliculas\noFights\*.mpg'; end
% if (nargin<2), path_videos_nofight='E:\Hockey\noFights\*.avi'; end
% if (nargin<2), path_videos_nofight='C:\Hockey\noFights\*.avi'; end
% if (nargin<2), path_videos_nofight='C:\UCF-101\noFights\*.avi'; end
% if (nargin<2), path_videos_nofight='E:\UCF-101\noFights\*.avi'; end
% if (nargin<2), path_videos_nofight='E:\UCF-101-pru500\no*.avi'; end
% if (nargin<2), path_videos_nofight='C:\UCF-101-pru500\no*.avi'; end
% if (nargin<2), path_videos_nofight='D:\HockeyPru\noFights\*.avi'; end

if (nargin<3), reSize=0; end
if (nargin<4), parallelize=0; end
if (nargin<5), level_threshold=-1; end
if (nargin<6), K=4; end
if (nargin<7), M=4; end
if (nargin<8), maxFiles=20; end

crossvalidation_folds=10;
% crossvalidation_folds=nsamplesF+nsamplesNF;  % Leave-one-out

%% Extract features from fight videos
[featuresFights,timeExtractFights,num_framesFights] = extractBlobFeatures(path_videos_fight, reSize, parallelize, level_threshold, K, maxFiles);


%% Extract features from no-fight videos
[featuresNoFights,timeExtractNoFights,num_framesNoFights] = extractBlobFeatures(path_videos_nofight, reSize, parallelize, level_threshold, K, maxFiles);


%% Select features from fight videos
[selectedFeaturesFights, timeSelectFights] = selectBlobFeatures(featuresFights, K, M);


%% Select features from no-fight videos
[selectedFeaturesNoFights, timeSelectNoFights] = selectBlobFeatures(featuresNoFights, K, M);


%% Classify
[R, timeClassify] = classifyBlob(selectedFeaturesFights,selectedFeaturesNoFights,crossvalidation_folds);


%% Save the results
mkdir('./results');
cd('./results');

dateCurrently=datestr(now);
tExtractFights=timeExtractFights/sum(num_framesFights);
tExtractNoFights=timeExtractNoFights/sum(num_framesNoFights);
tNuSVM=timeClassify(2);

fileName=[];
fileName=strcat(fileName,sprintf('%s v1a- reSize=%.1f tExtractF=%.3fs-f tExtractNF=%.3fs-f tSelectF=%.2fs tSelectNF=%.2fs tSuSVM=%.1fs maxFiles=%d',dateCurrently,reSize,tExtractFights,tExtractNoFights,timeSelectFights,timeSelectNoFights,tNuSVM,maxFiles));
fileName=strcat(fileName,'.mat');
fileName(strfind(fileName,':'))='.';

save(fileName);
clear;
cd('..');

end