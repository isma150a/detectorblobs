function [selected_features,time] = selectBlobFeatures(features, K, M)
fprintf('\n selectBlobDetector-v1 \n');

tic;
n_features = K + summation(K-1);
n_videos = length(features);
selected_features = zeros(n_videos, n_features*M);

for v=1:n_videos    % For each video
    if (length(features{v})==0), continue; end
        
    n_frames = size(features{v}.Frame,2);
    featuresVideo = zeros(n_frames, n_features);
    
    for f=1:n_frames % For each frame
        featuresVideo(f,:) = features{v}.Frame{f};
    end
    
    for fea = 1:n_features
        selected_features(v, (fea*M-M+1):fea*M) = hist(featuresVideo(:,fea),M) ./ n_frames;
    end
    
end
    

time=toc;

end