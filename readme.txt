Detector of Blobs algorithm is used and published in Fast Fight Detection paper published in the PloS One journal

Please cite our work:

-Serrano Gracia, I., Deniz Suarez, O., Bueno Garcia, G., & Kim, T. K. (2015). Fast fight detection. PLoS one, 10(4), e0120448-e0120448.


The project has dependencies with mmread and prtools (version 5) libraries. They are included on the project.

This prototype is tested on three different datasets: Movies, Hockey and UCF-101(explained in the original paper)

The Movies and Hockey datasets are available here: https://goo.gl/zwgN2T

The main file is the script.m where the dataset is selected and the algorithm is run
