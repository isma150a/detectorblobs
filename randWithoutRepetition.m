function m = randWithoutRepetitionM(max_value, max_files)
% Genera un vector aleaotorio de max_files elemtos sin repeticiones con valores entre [1
% max_value]
% m = randWithoutRepetitionM(max_value, max_files)

if max_value < max_files   % Every element
    m = 1:max_value;
    
else
    prob = max_files / max_value;   % Elements are only higher than prob
    m = zeros(1, max_files);
    m_discarted = zeros(1, max_value-max_files);
    counter_elem = 1;
    counter_discarted_elem = 1;
    
    for i=1:max_value
        if counter_elem > max_files
            m_discarted(counter_discarted_elem)=i;
            counter_discarted_elem=counter_discarted_elem+1;
        elseif counter_discarted_elem > (max_value-max_files)
            m(counter_elem)=i;
            counter_elem=counter_elem+1;
        
        elseif rand(1)<prob                         % selected element
            m(counter_elem)=i;
            counter_elem=counter_elem+1;
        else
            m_discarted(counter_discarted_elem)=i;  % no selected element
            counter_discarted_elem=counter_discarted_elem+1;
        end
    end    
end