function res = deleteNaN(in)

[dimX,dimY]=size(in);

for i=1:dimX
    for j=1:dimY
        if (isnan(in(i,j)))
            in(i,j)=0;
        end
        if (in(i,j)==Inf)
            in(i,j)=0;
        end
    end
end
res=in;

end