function [FEATURES,time,num_frames] = extractBlobFeatures(path_videos, reSize, parallelize, level_threshold, K, max_files)
fprintf('\n extractBlobDetector-v1 \n');

% To auto resize
MAX_WIDTH=420;
MAX_HEIGHT=300;  

% if (parallelize)
%     if (~(matlabpool('size')))
%         jm = findResource('scheduler', 'configuration', defaultParallelConfig);
%         jm.ClusterSize=min(parallelize, str2num(getenv('NUMBER_OF_PROCESSORS')));  % Full CPU usage !!
%         matlabpool(jm);
%     end
% else
%     matlabpool close force local;
% end

files=dir(path_videos);
[a,b,ext]=fileparts(path_videos);

% figure;
num_videos=min(max_files,length(files));
FEATURES=cell(num_videos,1);
nameVideos=cell(num_videos,1);

% FEATURES_AXU1=cell(num_videoNs,1);
% FEATURES_AUX2=cell(num_videoNs,1);

num_frames=zeros(1,num_videos);
reSizes=reSize*ones(num_videos);

% Cuando existen m�s videoNs que el max, se cogen eleatoriamente
AuxVideos=randWithoutRepetition(length(files),num_videos);


try
    tic;initial_time=toc;
    if(parallelize)
        parfor videoN=1:num_videos
            fprintf('\nProcessing video %s (%.1f%%)...',files(AuxVideos(videoN)).name,(videoN/num_videos)*100);
    %         % mmread toolbox can be downloaded from: http://www.mathworks.com/matlabcentral/fileexchange/8028
            Video=mmread(sprintf('%s\\%s',a,files(AuxVideos(videoN)).name));
            nameVideos{videoN}=files(AuxVideos(videoN)).name;

            if(reSize==0)   % auto resize
                reSizes(videoN)=1;
                if(Video.width>MAX_WIDTH && Video.height>MAX_HEIGHT)
                    while(Video.width*reSizes(videoN)>MAX_WIDTH && Video.height*reSizes(videoN)>MAX_HEIGHT)
                        reSizes(videoN)=reSizes(videoN)-0.1;
                    end
                end
                if(Video.width<MAX_WIDTH && Video.height<MAX_HEIGHT)
                    while(Video.width*reSizes(videoN)<MAX_WIDTH && Video.height*reSizes(videoN)<MAX_HEIGHT)
                        reSizes(videoN)=reSizes(videoN)+0.1;
                    end
                end
    %             fprintf(' reSize=%.1f%%',reSizes(videoN));
            end

            auxBeforeImage=rgb2gray(Video.frames(1).cdata);
            beforeImage=imresize(auxBeforeImage,reSizes(videoN));

            FEATURES{videoN}.Frame=cell(1,Video.nrFramesTotal-1);

            aux_nFrames=0;
            for frame=2:Video.nrFramesTotal
                auxImage=rgb2gray(Video.frames(frame).cdata);
                image=imresize(auxImage,reSizes(videoN));

                aux=detectBlobFeatures(beforeImage,image,level_threshold, K);

                FEATURES{videoN}.Frame{frame-1}=zeros(size(aux,1),size(aux,2));

                FEATURES{videoN}.Frame{frame-1}=aux;   %copy the features

                beforeImage=image;  
                aux_nFrames=aux_nFrames+1;
    %             pause;
            end
            num_frames(videoN)=aux_nFrames;
        end
    else
        %% Sin paralelizacion
        for videoN=1:num_videos
            fprintf('\nProcessing video %s (%.1f%%)...',files(AuxVideos(videoN)).name,(videoN/num_videos)*100);
    %         % mmread toolbox can be downloaded from: http://www.mathworks.com/matlabcentral/fileexchange/8028
            Video=mmread(sprintf('%s\\%s',a,files(AuxVideos(videoN)).name));
            nameVideos{videoN}=files(AuxVideos(videoN)).name;

            if(reSize==0)   % auto resize
                reSizes(videoN)=1;
                if(Video.width>MAX_WIDTH && Video.height>MAX_HEIGHT)
                    while(Video.width*reSizes(videoN)>MAX_WIDTH && Video.height*reSizes(videoN)>MAX_HEIGHT)
                        reSizes(videoN)=reSizes(videoN)-0.1;
                    end
                end
                if(Video.width<MAX_WIDTH && Video.height<MAX_HEIGHT)
                    while(Video.width*reSizes(videoN)<MAX_WIDTH && Video.height*reSizes(videoN)<MAX_HEIGHT)
                        reSizes(videoN)=reSizes(videoN)+0.1;
                    end
                end
    %             fprintf(' reSize=%.1f%%',reSizes(videoN));
            end

            auxBeforeImage=rgb2gray(Video.frames(1).cdata);
            beforeImage=imresize(auxBeforeImage,reSizes(videoN));

            FEATURES{videoN}.Frame=cell(1,Video.nrFramesTotal-1);

            aux_nFrames=0;
            for frame=2:Video.nrFramesTotal
                auxImage=rgb2gray(Video.frames(frame).cdata);
                image=imresize(auxImage,reSizes(videoN));

                aux=detectBlobFeatures(beforeImage,image,level_threshold, K);

                FEATURES{videoN}.Frame{frame-1}=zeros(size(aux,1),size(aux,2));

                FEATURES{videoN}.Frame{frame-1}=aux;   %copy the features

                beforeImage=image;  
                aux_nFrames=aux_nFrames+1;
    %             pause;
            end
            num_frames(videoN)=aux_nFrames;
        end
    end
catch err
    err.message
end

time=toc-initial_time;

end