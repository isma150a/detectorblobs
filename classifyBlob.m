function [R, time]=classifyBlob(selectedFeaturesFight, selectedFeaturesNoFight, crossvalidation_folds)

% Build the dataset...
nsamplesF=size(selectedFeaturesFight,1);
nsamplesNF=size(selectedFeaturesNoFight,1);
AllSamples=zeros(size(selectedFeaturesFight,1)+size(selectedFeaturesNoFight,1),size(selectedFeaturesFight,2));
AllLabels=zeros(size(selectedFeaturesFight,1)+size(selectedFeaturesNoFight,1),1);
AllSamples=[selectedFeaturesFight ; selectedFeaturesNoFight];
AllLabels=[repmat(1,nsamplesF,1);repmat(2,nsamplesNF,1)];
A=prdataset(AllSamples, AllLabels);
setprior(A,0);  

warning off
prwarning(0)

fprintf('\nUsing %d-fold cross-validation...\n', crossvalidation_folds);

% fprintf('\nTree classifier:');
% class=@treec; opts=['infcrit',-1];
% EvaluateClassifier(class, opts, A, crossvalidation_folds);



fprintf('\n\n************************************************\n Utilizando todas las caracterÝsticas:');

fprintf('\nKNN classifier:');
class=@knnc; opts=[];
[R(1,:), time_classifiers(1)] = EvaluateClassifier(class, opts, A, crossvalidation_folds);

% fprintf('\nNUSVM classifier:');
% class=@nusvc; opts=[];
% [R(2,:), time_classifiers(2)] = EvaluateClassifier(class, opts, A, crossvalidation_folds);

% fprintf('\nSVM classifier:');
% class=@svc; opts=[];
% [R(2,:), time_classifiers(2)] = EvaluateClassifier(class, opts, A, crossvalidation_folds);

fprintf('\nAdaBoost classifier:');
class=@adaboostc; opts=[];
[R(3,:), time_classifiers(3)] = EvaluateClassifier(class, opts, A, crossvalidation_folds);

% tic; fprintf('\nLDC classifier:'); % good work
% class=@ldc; opts=[];
% [R(4,:),time_classifiers(4)] = EvaluateClassifier(class, opts, A, crossvalidation_folds);

% fprintf('\nSV classifier:');    % very slow
% class=@svc; opts=[];
% [R(5,:), curvaROC_x(5,:), curvaROC_y(5,:),time_classifiers(5)] = EvaluateClassifier(class, opts, A, crossvalidation_folds);

% fprintf('\nQD classifier:');    % work
% class=@qdc; opts=[];
% [R(6,:), curvaROC_x(6,:), curvaROC_y(6,:),time_classifiers(6)] = EvaluateClassifier(class, opts, A, crossvalidation_folds);

% fprintf('\nParzen classifier:');    %work
% class=@parzenc; opts=[];
% [R(7,:), curvaROC_x(7,:), curvaROC_y(7,:),time_classifiers(7)] = EvaluateClassifier(class, opts, A, crossvalidation_folds);

% fprintf('\nTree classifier:');     %work
% class=@treec; opts=[];
% [R(8,:), curvaROC_x(8,:), curvaROC_y(8,:),time_classifiers(8)] = EvaluateClassifier(class, opts, A, crossvalidation_folds);

% fprintf('\nRandom Forest classifier:');     %work
% class=@randomforestc; opts=[];
% [R(9,:), curvaROC_x(9,:), curvaROC_y(9,:),time_classifiers(9)] = EvaluateClassifier(class, opts, A, crossvalidation_folds);

% fprintf('\nRandom Forest (Mila and Gloria) classifier:')
% tic; [aux_NLAB_OUT,res,accu,pDetection,fPositives]=crossval_randomForest(A,crossvalidation_folds);
% time_classifiers(10)=toc;
% R(10,:)=[accu,pDetection/100,fPositives/100,pDetection,fPositives];
% [curvaROC_x(10,:), curvaROC_y(10,:)]=ROC_prob(A.nlab,res);

time=time_classifiers;

warning on
prwarning(1)
close all
prwaitbar off;
fprintf('\n');





function [r, time_classify]=EvaluateClassifier(class, opts, A, crossvalidation_folds)
tic;
AllSamples=A.data;
AllLabels=A.nlab;

evalc('[ERR,CERR,NLAB_OUT]=prcrossval(A,feval(class,[],opts),crossvalidation_folds);');
rr=100-(100*ERR);

mConfusion=confmat(A.nlab,NLAB_OUT);
try
    nPositivas=mConfusion(1,1)+mConfusion(1,2);
    nNegativas=mConfusion(2,1)+mConfusion(2,2);
catch    
    nPositivas=mConfusion(1,1);
    nNegativas=mConfusion(2,1);
end
positiveDetection=(mConfusion(1,1)/nPositivas)*100;
falsePositives=(mConfusion(2,1)/nNegativas)*100;
rr2=(positiveDetection*nPositivas+(100-falsePositives)*nNegativas)/(nPositivas+nNegativas);
rr=rr2;

fprintf('\n Recognition rate: %.1f%%', rr);
fprintf('\n  -Positive detection: %.1f%%', positiveDetection);
fprintf('\n  -False positives: %.1f%%\n', falsePositives);

% % evalc('folds=prcrossval(A,feval(class,[],opts),crossvalidation_folds,0);');
% nTypeA = size(find(A.nlab==1),1);
% nTypeB = size(find(A.nlab==2),1);
% foldsA = RandWithoutRepetition(nTypeA,nTypeA);
% foldsA = mod(foldsA,crossvalidation_folds);
% foldsB = RandWithoutRepetition(nTypeB,nTypeB);
% foldsB = mod(foldsB,crossvalidation_folds);
% folds = [foldsA foldsB];
% 
% curva_Y=zeros(crossvalidation_folds, 101);
% curva_X=zeros(crossvalidation_folds, 101);
% for i=1:crossvalidation_folds
%     iTr=find(folds~=i);
%     iTe=find(folds==i);
%     Tr=prdataset(AllSamples(iTr,:),AllLabels(iTr,:));
%     Te=prdataset(AllSamples(iTe,:),AllLabels(iTe,:));
%     evalc('W=feval(class,Tr,opts);');
% %     AUC(i)=testauc(Te*W);
%     r=roc(Te*W); 
% %     figure;plote(r);
% 
% %     curva_Y(i,:)=1-r.error;
% %     curva_X(i,:)=r.xvalues;
%     curva_Y(i,:)=1-r.xvalues;
%     curva_X(i,:)=r.error;
% end
% curvaROC_y=mean(curva_Y);
% curvaROC_x=mean(curva_X);

time_classify=toc;
fprintf(' Time=%.2f seconds\n',time_classify);
r=[rr CERR positiveDetection falsePositives]; % error of each class

