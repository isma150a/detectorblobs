function sub_img = subImagen(img, center_x, center_y, dim_x, dim_y)
    %% sub_img = subImagen(img, center_x, center_y, dim_x, dim_y)
    % Obtain the sub imagen (sub_img) from the center and dimension (dim) of imagen (img)
    
    [dim_img_x, dim_img_y]= size(img);
    
    if(center_x > dim_img_x || center_x <= 0 || center_y > dim_img_y || center_y <= 0)
        %% Center outsite the img
        sub_img = [];
        
    elseif (dim_x >= dim_img_x || dim_y >= dim_img_y)
        %% If sub_imagen dimesion is lower than img
        sub_img = img;    
        
    else
        min_x = center_x - floor(dim_x/2);
        max_x = center_x + round(dim_x/2) - 1;
        if (min_x <= 0)
            max_x = max_x + abs(min_x) +1 ;
            min_x = 1;
        end
        if (max_x > dim_img_x)
            min_x = min_x - (max_x-dim_img_x);
            max_x = dim_img_x;
        end

        min_y = center_y - floor(dim_y/2);
        max_y = center_y + round(dim_y/2) - 1;
        if (min_y <= 0)
            max_y = max_y + abs(min_y)+ 1;
            min_y = 1;
        end
        if (max_y > dim_img_y)
            min_y = min_y - (max_y-dim_img_y);
            max_y = dim_img_y;
        end

        sub_img = img(min_x:max_x,min_y:max_y);
    end   
%     subplot(1,2,1); imshow(img);
%     subplot(1,2,2); imshow(sub_img);
%     aux_img = img;
%     show(aux_img);hold on;
%     plot(center_y,center_x,'rx');
%     pause(0.001);

end