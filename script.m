%% Setting 
fprintf('Version 1a of detectorBlobs');
addpath(genpath('./prtools/'));
addpath(genpath('./mmread/'));


level_threshold=0.2;    % with -1 to use Otsu threshold
K=8;                    % K largest blobs
M=8;
re_size=0;
parallelize=3;          %0=no parallelize   n=parallelize with n workers

max_files=100;



%% % First dataset
path_videos_fight='G:\Movies\fights\*.avi';
path_videos_nofight='G:\Movies\noFights\*.mpg';
fightBlobDetector(path_videos_fight, path_videos_nofight, re_size, parallelize, level_threshold, K, M, max_files);




%% % Second dataset
% path_videos_fight='G:\Hockey\fights\*.avi';
% path_videos_nofight='G:\Hockey\noFights\*.avi';
% fightBlobDetector(path_videos_fight, path_videos_nofight, re_size, parallelize, level_threshold, K, M, max_files);



% %% % Third dataset
% path_videos_fight='G:\UCF-101\figths\*.avi';
% path_videos_nofight='G:\UCF-101\noFights\*.avi';
% fightBlobDetector(path_videos_fight, path_videos_nofight, re_size, parallelize, level_threshold, K, M, max_files);



