function[R]=drawLine(B,x1,y1,x2,y2)
%Dibuja una linea blanca en la imagen de entrada entre los dos puntos
%correspondientes a las coordenadas introducidas.
%   ->A=imagen de entrada 
%   ->x1=Coordenada X pto 1
%   ->y1=Coordenada Y pto 1
%   ->x2=Coordenada X pto 2
%   ->y2=Coordenada Y pto 2
%Los ejes de coordenadas estar�n en la esquina superior izquierda

%Leemos la imagen. Usamos un if para permitirnos entrar a la funcion a
%traves de una imagen o de la matriz de pixeles directamente
if (size(B,1)==1)
   R=imread(B);
else
    R=B;
end

x1 = round(x1);
y1 = round(y1);
x2 = round(x2);
y2 = round(y2);

%La ecuacion de una recta: '(y-y1)=(y2-y1)/(x2-x1)*(x-x1)' . Recorremos la
%matriz, y los puntos que cumplan la ecuacion los ponemos a 255.
%Calculamos aparte la pendiente
m=(y2-y1)/(x2-x1);

%Si el valor de la pendiente no es entero, no podemos dibujar una linea
%recta entre dos puntos. Lo comprobamos y en caso de qe la pendiente sea
%decimal devolvemos un mensaje de error.
% if(rem(m,1)~=0)
%     error('Las coordenadas no son validas')
% end

for i=min(y1,y2):max(y1,y2)
    for j=min(x1,x2):max(x1,x2)
        if(i>=m*(j-x1)+y1-1 && i<=m*(j-x1)+y1+1)
            R(i,j)=1;
        else
            R(i,j)=R(i,j);
        end
    end
end
% R=uint8(R);

return