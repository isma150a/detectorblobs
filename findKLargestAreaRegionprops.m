function positions = findKLargestAreaRegionprops(input, K)
    % positions = findKLargest(input, K)
    %
    % find de K largest position for the input structure regionprops
    
    positions = zeros(1,K);
    input_aux = input;
    
    for i=1:K
        pos = posMaxRegionprops(input_aux);
        positions(i) = pos;
        if (pos ~= -1)
%             input_aux(pos).Area = 0;
            input_aux(pos).Area = 0;
        end
    end
    
    

end